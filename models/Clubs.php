<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clubs".
 *
 * @property int $ID
 * @property string|null $Nombre
 * @property string|null $Ubicacion
 * @property string|null $F_Apertura
 * @property string|null $F_Clausura
 * @property int|null $ID_Artista
 * @property int|null $ID_Evento
 * @property int|null $ID_Servicios
 * @property int|null $ID_Valoracion
 *
 * @property Artistas $artista
 * @property Eventos $evento
 * @property Servicios $servicios
 */
class Clubs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clubs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['F_Apertura', 'F_Clausura'], 'safe'],
            [['ID_Artista', 'ID_Evento', 'ID_Servicios', 'ID_Valoracion'], 'integer'],
            [['Nombre'], 'string', 'max' => 25],
            [['Ubicacion'], 'string', 'max' => 50],
            [['ID_Artista'], 'exist', 'skipOnError' => true, 'targetClass' => Artistas::class, 'targetAttribute' => ['ID_Artista' => 'ID']],
            [['ID_Evento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::class, 'targetAttribute' => ['ID_Evento' => 'ID']],
            [['ID_Servicios'], 'exist', 'skipOnError' => true, 'targetClass' => Servicios::class, 'targetAttribute' => ['ID_Servicios' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Ubicacion' => 'Ubicacion',
            'F_Apertura' => 'F Apertura',
            'F_Clausura' => 'F Clausura',
            'ID_Artista' => 'Id Artista',
            'ID_Evento' => 'Id Evento',
            'ID_Servicios' => 'Id Servicios',
            'ID_Valoracion' => 'Id Valoracion',
        ];
    }

    /**
     * Gets query for [[Artista]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArtista()
    {
        return $this->hasOne(Artistas::class, ['ID' => 'ID_Artista']);
    }

    /**
     * Gets query for [[Evento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvento()
    {
        return $this->hasOne(Eventos::class, ['ID' => 'ID_Evento']);
    }

    /**
     * Gets query for [[Servicios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasOne(Servicios::class, ['ID' => 'ID_Servicios']);
    }
}
