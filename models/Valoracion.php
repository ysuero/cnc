<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valoracion".
 *
 * @property int $ID
 * @property int|null $Nmuero Estrellas
 */
class Valoracion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valoracion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nmuero Estrellas'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nmuero Estrellas' => 'Nmuero Estrellas',
        ];
    }
}
