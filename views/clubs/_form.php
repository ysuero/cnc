<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Clubs $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="clubs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Ubicacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'F_Apertura')->textInput() ?>

    <?= $form->field($model, 'F_Clausura')->textInput() ?>

    <?= $form->field($model, 'ID_Artista')->textInput() ?>

    <?= $form->field($model, 'ID_Evento')->textInput() ?>

    <?= $form->field($model, 'ID_Servicios')->textInput() ?>

    <?= $form->field($model, 'ID_Valoracion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
