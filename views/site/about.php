<?php
use yii\helpers\Html;

$this->title = 'Conocenos un Poco Mas';

// Incluye el archivo CSS
$this->registerCssFile('@web/css/about.css');

?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="perfil">
        <div><h4>Desarrollador</h4></div>
        <div class="box">
            <?= Html::img('@web/imagen.jpg', ['alt' => 'Imagen']) ?>
            <div>
                <h5>Yoel Suero</h5>
                <p>Desarollador en practicas, creador de esta aplicacion.
                    "Esta aplicacion es un pequeño proyecto aun inacabado, 
                    con mejoras a futuro, Todas la opiniones son bienvenidas"
                </p>
            </div>
        </div>
        <div class="contactanos">
            <h2>Contacta con nosotros:</h2>
            <?= Html::img('@web/telefono.png', ['alt' => 'Teléfono']) ?>
            <p>601316010</p>
        </div>
        <div class="redes-sociales">
            <h2>Siguenos en nuestras Redes Sociales:</h2>
            <p>No te pierdas todo nuestro camino, nuevas noticias, eventos y ponte al dia con nuestro trabajo en nuestras redes sociales</p>
            <?= Html::a(Html::img('@web/facebook.png', ['alt' => 'Facebook']), '#', ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
            <p>Facebook</p>
            <?= Html::a(Html::img('@web/twitter.png', ['alt' => 'Twitter']), '#', ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
            <p>Twitter</p>
            <?= Html::a(Html::img('@web/instagram.png', ['alt' => 'Instagram']), '#', ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
            <p>Instagram</p>
        </div>
    </div>
</div>