<?php

use yii\helpers\Html;

$this->title = 'Clubs Nocturnos';
?>

<h1><?= Html::encode($this->title) ?></h1>

<?= Html::a('Ordenar por Nombre', ['site/clubs', 'sort' => 'nombre']) ?>
<?= Html::a('Ordenar por Valoración', ['site/clubs', 'sort' => 'valoracion']) ?>

<div class="club-list">
    <?php foreach ($clubs as $club): ?>
        <div class="club-box">
            <?= Html::img($club->imagen, ['class' => 'club-image']) ?>
            <div class="club-details">
                <h2><?= Html::encode($club->nombre) ?></h2>
                <p><?= Html::encode($club->ubicacion) ?></p>
                <p>Horario: <?= Html::encode($club->hora_apertura) ?> - <?= Html::encode($club->hora_clausura) ?></p>
                <p>Valoración: <?= Html::encode($club->valoracion) ?></p>
            </div>
        </div>
    <?php endforeach; ?>
</div>