<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Contacto';
$this->registerCssFile('@web/css/contact.css');
?>

<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="contact-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
