<?php

/** @var yii\web\View $this */

$this->title = 'CNC - Clubs Nocturnos';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">¡Bienvenido a CNC!</h1>

        <p class="lead">Explora una experiencia nocturna única con nosotros.</p>

        <p><a class="btn btn-lg btn-success" href="#">Descubre más</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <img src="../../web/img/club1.jpg" alt="Club Nocturno 1" class="img-fluid">
                <h2>Club Nocturno 1</h2>

                <p>Disfruta de la mejor música y ambiente en nuestro club nocturno.</p>

                <p><a class="btn btn-outline-secondary" href="#">Ver más &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <img src="../../web/img/club2.jpg" alt="Club Nocturno 2" class="img-fluid">
                <h2>Club Nocturno 2</h2>
                
                <p>Vive la noche al máximo con nuestras increíbles fiestas temáticas.</p>

                <p><a class="btn btn-outline-secondary" href="#">Ver más &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <img src="../../web/img/club3.jpg" alt="Club Nocturno 3" class="img-fluid">
                <h2>Club Nocturno 3</h2>

                <p>Descubre los mejores cócteles y bebidas en nuestro exclusivo bar.</p>

                <p><a class="btn btn-outline-secondary" href="#">Ver más &raquo;</a></p>
            </div>
        </div>

    </div>
</div>