<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Valoracion $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="valoracion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nmuero Estrellas')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
